<div class="alert alert-<?php echo $messageType; ?> fade in">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <?php echo $message; ?>
</div>
